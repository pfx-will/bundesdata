### **Streamlit Dashboard**

**Data Collection:**

Bundesliga game data taken from [football-data.org]() via API. This is designed to be scheduled to run at most Daily.

Bundesliga team information scraped from [Transfermarkt](https://www.transfermarkt.co.uk/1-bundesliga/startseite/wettbewerb/L1)

**Data Storage**

Designed to run with a Postgres database, this project can be setup using the [MAKEFILE](https://gitlab.com/pfx-will/bundesdata/-/blob/main/MAKEFILE) with data added during collection using pyscopg2.

**Dashboard**

The dashboard is powered by Streamlit with visualizations empowered using altair, bootstrap and streamlit.